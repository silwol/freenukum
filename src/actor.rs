// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>

mod accesscarddoor;
mod accesscardslot;
mod acme;
mod backgroundanimation;
mod balloon;
mod bomb;
mod camera;
mod conveyor;
mod door;
mod electricarc;
mod elevator;
mod exitdoor;
mod expandingfloor;
mod fan;
mod fire;
mod firewheelbot;
mod flyingball;
mod gloveslot;
mod hostileshot;
mod item;
mod jumpbot;
mod key;
mod keyhole;
mod mill;
mod minejumping;
mod minelying;
mod notebook;
mod particle;
mod placeholder;
mod robot;
mod rocket;
mod score;
mod shootablewall;
mod singleanimation;
mod sodaflying;
mod spikes;
mod surveillancescreen;
mod tankbot;
mod teleporter;
mod unstablefloor;
mod wallcrawler;

use crate::{
    game::{GameCommandQueue, GameCommands, LevelCommandProcessor},
    geometry::RectExt,
    hero::Hero,
    infobox::InfoMessageQueue,
    level::{tiles::LevelTiles, BackgroundTileStrategy, PlayState},
    rendering::Renderer,
    sound::SoundIndex,
    HorizontalDirection, KeyColor, Result, Sizes,
};
use sdl2::rect::{Point, Rect};
pub(crate) use {
    accesscarddoor::AccessCardDoor,
    accesscardslot::AccessCardSlot,
    acme::Acme,
    backgroundanimation::{BackgroundAnimation, BackgroundAnimationType},
    balloon::Balloon,
    bomb::Bomb,
    camera::Camera,
    conveyor::Conveyor,
    door::Door,
    electricarc::ElectricArc,
    elevator::Elevator,
    exitdoor::ExitDoor,
    expandingfloor::ExpandingFloor,
    fan::Fan,
    fire::Fire,
    firewheelbot::FireWheelBot,
    flyingball::FlyingBall,
    gloveslot::GloveSlot,
    hostileshot::HostileShot,
    item::{
        BoxBlueContent, BoxGreyContent, BoxRedContent, Item, ItemType,
    },
    jumpbot::JumpBot,
    key::Key,
    keyhole::KeyHole,
    mill::Mill,
    minejumping::MineJumping,
    minelying::MineLying,
    notebook::NoteBook,
    particle::{Particle, ParticleColor},
    placeholder::PlaceHolder,
    robot::Robot,
    rocket::Rocket,
    score::{Score, ScoreType},
    shootablewall::ShootableWall,
    singleanimation::{SingleAnimation, SingleAnimationType},
    sodaflying::SodaFlying,
    spikes::{SpikeType, Spikes},
    surveillancescreen::SurveillanceScreen,
    tankbot::TankBot,
    teleporter::{Teleporter, TeleporterIndex},
    unstablefloor::UnstableFloor,
    wallcrawler::WallCrawler,
};

#[derive(Debug)]
pub(crate) enum Actor {
    FireWheelBot(FireWheelBot),
    PlaceHolder(PlaceHolder),
    JumpBot(JumpBot),
    MineJumping(MineJumping),
    MineLying(MineLying),
    Robot(Robot),
    SingleAnimation(SingleAnimation),
    FlyingBall(FlyingBall),
    TankBot(TankBot),
    WallCrawler(WallCrawler),
    Camera(Camera),
    Particle(Particle),
    Rocket(Rocket),
    Bomb(Bomb),
    ExitDoor(ExitDoor),
    NoteBook(NoteBook),
    SurveillanceScreen(SurveillanceScreen),
    HostileShot(HostileShot),
    SodaFlying(SodaFlying),
    UnstableFloor(UnstableFloor),
    ExpandingFloor(ExpandingFloor),
    Conveyor(Conveyor),
    Fan(Fan),
    Teleporter(Teleporter),
    Item(Item),
    Balloon(Balloon),
    AccessCardSlot(AccessCardSlot),
    GloveSlot(GloveSlot),
    Key(Key),
    KeyHole(KeyHole),
    Door(Door),
    ShootableWall(ShootableWall),
    Elevator(Elevator),
    Acme(Acme),
    Fire(Fire),
    Mill(Mill),
    ElectricArc(ElectricArc),
    AccessCardDoor(AccessCardDoor),
    Spikes(Spikes),
    Score(Score),
    BackgroundAnimation(BackgroundAnimation),
}

impl Actor {
    fn as_actor_ext_mut(&mut self) -> &mut dyn ActorExt {
        match self {
            Actor::FireWheelBot(ref mut a) => a,
            Actor::PlaceHolder(ref mut a) => a,
            Actor::JumpBot(ref mut a) => a,
            Actor::MineJumping(ref mut a) => a,
            Actor::MineLying(ref mut a) => a,
            Actor::Robot(ref mut a) => a,
            Actor::SingleAnimation(ref mut a) => a,
            Actor::FlyingBall(ref mut a) => a,
            Actor::TankBot(ref mut a) => a,
            Actor::WallCrawler(ref mut a) => a,
            Actor::Camera(ref mut a) => a,
            Actor::Particle(ref mut a) => a,
            Actor::Rocket(ref mut a) => a,
            Actor::Bomb(ref mut a) => a,
            Actor::ExitDoor(ref mut a) => a,
            Actor::NoteBook(ref mut a) => a,
            Actor::SurveillanceScreen(ref mut a) => a,
            Actor::HostileShot(ref mut a) => a,
            Actor::SodaFlying(ref mut a) => a,
            Actor::UnstableFloor(ref mut a) => a,
            Actor::ExpandingFloor(ref mut a) => a,
            Actor::Conveyor(ref mut a) => a,
            Actor::Fan(ref mut a) => a,
            Actor::Teleporter(ref mut a) => a,
            Actor::Item(ref mut a) => a,
            Actor::Balloon(ref mut a) => a,
            Actor::AccessCardSlot(ref mut a) => a,
            Actor::GloveSlot(ref mut a) => a,
            Actor::Key(ref mut a) => a,
            Actor::KeyHole(ref mut a) => a,
            Actor::Door(ref mut a) => a,
            Actor::ShootableWall(ref mut a) => a,
            Actor::Elevator(ref mut a) => a,
            Actor::Acme(ref mut a) => a,
            Actor::Fire(ref mut a) => a,
            Actor::Mill(ref mut a) => a,
            Actor::ElectricArc(ref mut a) => a,
            Actor::AccessCardDoor(ref mut a) => a,
            Actor::Spikes(ref mut a) => a,
            Actor::Score(ref mut a) => a,
            Actor::BackgroundAnimation(ref mut a) => a,
        }
    }

    fn as_actor_ext(&self) -> &dyn ActorExt {
        match self {
            Actor::FireWheelBot(ref a) => a,
            Actor::PlaceHolder(ref a) => a,
            Actor::JumpBot(ref a) => a,
            Actor::MineJumping(ref a) => a,
            Actor::MineLying(ref a) => a,
            Actor::Robot(ref a) => a,
            Actor::SingleAnimation(ref a) => a,
            Actor::FlyingBall(ref a) => a,
            Actor::TankBot(ref a) => a,
            Actor::WallCrawler(ref a) => a,
            Actor::Camera(ref a) => a,
            Actor::Particle(ref a) => a,
            Actor::Rocket(ref a) => a,
            Actor::Bomb(ref a) => a,
            Actor::ExitDoor(ref a) => a,
            Actor::NoteBook(ref a) => a,
            Actor::SurveillanceScreen(ref a) => a,
            Actor::HostileShot(ref a) => a,
            Actor::SodaFlying(ref a) => a,
            Actor::UnstableFloor(ref a) => a,
            Actor::ExpandingFloor(ref a) => a,
            Actor::Conveyor(ref a) => a,
            Actor::Fan(ref a) => a,
            Actor::Teleporter(ref a) => a,
            Actor::Item(ref a) => a,
            Actor::Balloon(ref a) => a,
            Actor::AccessCardSlot(ref a) => a,
            Actor::GloveSlot(ref a) => a,
            Actor::Key(ref a) => a,
            Actor::KeyHole(ref a) => a,
            Actor::Door(ref a) => a,
            Actor::ShootableWall(ref a) => a,
            Actor::Elevator(ref a) => a,
            Actor::Acme(ref a) => a,
            Actor::Fire(ref a) => a,
            Actor::Mill(ref a) => a,
            Actor::ElectricArc(ref a) => a,
            Actor::AccessCardDoor(ref a) => a,
            Actor::Spikes(ref a) => a,
            Actor::Score(ref a) => a,
            Actor::BackgroundAnimation(ref a) => a,
        }
    }
}

impl ActorExt for Actor {
    fn hero_can_interact(&self, hero: &Hero) -> bool {
        self.as_actor_ext().hero_can_interact(hero)
    }

    fn hero_interact_start(&mut self, p: HeroInteractStartParameters) {
        self.as_actor_ext_mut().hero_interact_start(p)
    }

    fn hero_interact_end(&mut self, p: HeroInteractEndParameters) {
        self.as_actor_ext_mut().hero_interact_end(p)
    }

    fn act(&mut self, p: ActParameters) {
        self.as_actor_ext_mut().act(p)
    }

    fn render(&mut self, p: RenderParameters) -> Result<()> {
        self.as_actor_ext_mut().render(p)
    }

    fn can_get_shot(&self) -> bool {
        self.as_actor_ext().can_get_shot()
    }

    fn shot(&mut self, p: ShotParameters) -> ShotProcessing {
        self.as_actor_ext_mut().shot(p)
    }

    fn position(&self) -> Rect {
        self.as_actor_ext().position()
    }

    fn is_in_foreground(&self) -> bool {
        self.as_actor_ext().is_in_foreground()
    }

    fn hurts_hero(&self, hero: &Hero) -> bool {
        self.as_actor_ext().hurts_hero(hero)
    }

    fn is_alive(&self) -> bool {
        self.as_actor_ext().is_alive()
    }

    fn acts_while_invisible(&self) -> bool {
        self.as_actor_ext().acts_while_invisible()
    }

    fn can_receive_message(&self, message_type: ActorMessageType) -> bool {
        self.as_actor_ext().can_receive_message(message_type)
    }

    fn receive_message(&mut self, p: ReceiveMessageParameters) {
        self.as_actor_ext_mut().receive_message(p)
    }

    fn background_tile_strategy(&self) -> BackgroundTileStrategy {
        self.as_actor_ext().background_tile_strategy()
    }
}

#[derive(Debug)]
pub struct ActorsList {
    pub(crate) actors: Vec<Actor>,
    interaction_target: Option<usize>,
}

impl Default for ActorsList {
    fn default() -> Self {
        Self::new()
    }
}

impl ActorsList {
    pub fn new() -> Self {
        ActorsList {
            actors: Vec::new(),
            interaction_target: None,
        }
    }

    pub fn count(&self) -> usize {
        self.actors.len()
    }

    pub fn remove_dead(&mut self) {
        let mut i = 0;
        while i < self.actors.len() {
            match self.actors.get(i) {
                Some(a) if !a.is_alive() => {
                    self.actors.remove(i);
                    self.interaction_target = match self.interaction_target
                    {
                        Some(index) if index == i => None,
                        Some(index) if index > i => Some(index - 1),
                        Some(index) => Some(index),
                        None => None,
                    };
                }
                _ => {
                    i += 1;
                }
            }
        }
    }

    pub fn send_message(
        &mut self,
        message: ActorMessageType,
        sizes: &dyn Sizes,
        hero: &mut Hero,
        tiles: &mut LevelTiles,
    ) {
        for actor in self
            .actors
            .iter_mut()
            .filter(|a| a.can_receive_message(message))
        {
            let p = ReceiveMessageParameters {
                message,
                sizes,
                hero,
                tiles,
            };
            actor.receive_message(p);
        }
    }

    pub fn process_shot(
        &mut self,
        shot_position: Rect,
        sizes: &dyn Sizes,
        tiles: &mut LevelTiles,
        game_commands: &mut dyn GameCommands,
        hero: &mut Hero,
        actor_message_queue: &mut ActorMessageQueue,
    ) -> bool {
        for actor in self.actors.iter_mut() {
            let p = ShotParameters {
                sizes,
                tiles,
                game_commands,
                hero,
                actor_message_queue,
            };
            if actor.can_get_shot()
                && shot_position.touches(actor.position())
                && actor.shot(p) == ShotProcessing::Absorb
            {
                return true;
            }
        }
        false
    }

    pub fn start_interaction(
        &mut self,
        play_state: &mut PlayState,
        hero: &mut Hero,
        game_commands: &mut dyn GameCommands,
        info_message_queue: &mut InfoMessageQueue,
        actor_message_queue: &mut ActorMessageQueue,
    ) {
        let new_interactor = self.actors.iter().position(|actor| {
            actor.hero_can_interact(hero)
                && hero.position.geometry.touches(actor.position())
        });

        if let Some(i) = new_interactor {
            self.end_interaction(play_state, hero);

            let actor = self.actors.get_mut(i).unwrap();
            let p = HeroInteractStartParameters {
                play_state,
                hero,
                game_commands,
                info_message_queue,
                actor_message_queue,
            };
            self.interaction_target = Some(i);
            actor.hero_interact_start(p);
        }
    }

    pub fn end_interaction(
        &mut self,
        play_state: &mut PlayState,
        hero: &mut Hero,
    ) {
        if let Some(i) = self.interaction_target.take() {
            if let Some(actor) = self.actors.get_mut(i) {
                let p = HeroInteractEndParameters { play_state, hero };
                actor.hero_interact_end(p);
            }
        }
    }

    #[allow(clippy::too_many_arguments)]
    pub fn act(
        &mut self,
        sizes: &dyn Sizes,
        tiles: &mut LevelTiles,
        hero: &mut Hero,
        game_commands: &mut GameCommandQueue,
        play_state: &mut PlayState,
        play_sounds: &mut Vec<SoundIndex>,
        visible_rect: Rect,
        actor_message_queue: &mut ActorMessageQueue,
    ) {
        let mut actors_hurting_hero = 0usize;
        for actor in self.actors.iter_mut() {
            if actor.acts_while_invisible()
                || actor.position().has_intersection(visible_rect)
            {
                let p = ActParameters {
                    sizes,
                    tiles,
                    hero,
                    game_commands,
                    play_state,
                    actor_message_queue,
                };
                actor.act(p);
                if actor.is_alive() && actor.hurts_hero(hero) {
                    actors_hurting_hero += 1;
                }
            }
        }
        self.remove_dead();

        let mut command_processor = LevelCommandProcessor {
            sizes,
            tiles,
            actors: self,
            play_sounds,
            copy_background: false,
        };

        game_commands.process(&mut command_processor);
        hero.gets_hurt = actors_hurting_hero > 0;
    }

    pub fn render_background_actors(
        &mut self,
        renderer: &mut dyn Renderer,
        sizes: &dyn Sizes,
        draw_collision_bounds: bool,
        visible_rect: Rect,
    ) -> Result<()> {
        self.actors
            .iter_mut()
            .filter(|actor| {
                !actor.is_in_foreground()
                    && actor.position().has_intersection(visible_rect)
            })
            .try_for_each(|actor| {
                let p = RenderParameters { renderer, sizes };
                let res = actor.render(p);
                if res.is_ok() && draw_collision_bounds {
                    let color = crate::collision_bounds_color();
                    renderer.draw_rect(actor.position(), color)?;
                }
                res
            })
    }

    pub fn render_foreground_actors(
        &mut self,
        renderer: &mut dyn Renderer,
        sizes: &dyn Sizes,
        draw_collision_bounds: bool,
        visible_rect: Rect,
    ) -> Result<()> {
        self.actors
            .iter_mut()
            .filter(|actor| {
                actor.is_in_foreground()
                    && actor.position().has_intersection(visible_rect)
            })
            .try_for_each(|actor| {
                let p = RenderParameters { renderer, sizes };
                let res = actor.render(p);
                if res.is_ok() && draw_collision_bounds {
                    let color = crate::collision_bounds_color();
                    renderer.draw_rect(actor.position(), color)?;
                }
                res
            })
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum ShotProcessing {
    Absorb,
    Ignore,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ActorType {
    FireWheelBot,
    FlameGnomeBot,
    FlyingBot,
    JumpBot,
    HelicopterBot,
    RabbitoidBot,
    MineJumping,
    MineLying,
    Robot,
    SingleAnimation(SingleAnimationType),
    FlyingBall(usize),
    TankBot,
    WallCrawler(HorizontalDirection),
    DrProton,
    Camera,
    Particle(ParticleColor),
    Rocket,
    Bomb,
    Water,
    ExitDoor,
    NoteBook,
    SurveillanceScreen,
    HostileShot(HorizontalDirection),
    SodaFlying,
    UnstableFloor,
    ExpandingFloor,
    ConveyorRightEnd(HorizontalDirection),
    Fan(HorizontalDirection),
    StoneBackground,
    Teleporter(TeleporterIndex),
    FenceBackground,
    Screen,
    Item(ItemType),
    Balloon,
    AccessCardSlot,
    GloveSlot,
    Key(KeyColor),
    Keyhole(KeyColor),
    Door(KeyColor),
    ShootableWall,
    Elevator,
    Acme,
    Fire(HorizontalDirection),
    Mill,
    ElectricArc,
    AccessCardDoor,
    Spikes(SpikeType),
    Score(ScoreType),
    BackgroundAnimation(BackgroundAnimationType),
}

impl ActorType {
    pub(crate) fn create_actor_boxed(
        &self,
        p: Point,
        sz: &dyn Sizes,
        t: &mut LevelTiles,
    ) -> Actor {
        match self {
            ActorType::FireWheelBot => FireWheelBot::create(p, sz, t),
            ActorType::FlameGnomeBot => {
                PlaceHolder::create_with_details(*self, p, sz, t)
            }
            ActorType::FlyingBot => {
                PlaceHolder::create_with_details(*self, p, sz, t)
            }
            ActorType::JumpBot => JumpBot::create(p, sz, t),
            ActorType::HelicopterBot => {
                PlaceHolder::create_with_details(*self, p, sz, t)
            }
            ActorType::RabbitoidBot => {
                PlaceHolder::create_with_details(*self, p, sz, t)
            }
            ActorType::MineJumping => MineJumping::create(p, sz, t),
            ActorType::MineLying => MineLying::create(p, sz, t),
            ActorType::Robot => Robot::create(p, sz, t),
            ActorType::SingleAnimation(animation_type) => {
                SingleAnimation::create_with_details(
                    *animation_type,
                    p,
                    sz,
                    t,
                )
            }
            ActorType::FlyingBall(i) => {
                FlyingBall::create_with_details(*i, p, sz, t)
            }
            ActorType::TankBot => TankBot::create(p, sz, t),
            ActorType::WallCrawler(direction) => {
                WallCrawler::create_with_details(*direction, p, sz, t)
            }
            ActorType::DrProton => {
                PlaceHolder::create_with_details(*self, p, sz, t)
            }
            ActorType::Camera => Camera::create(p, sz, t),
            ActorType::Particle(color) => {
                Particle::create_with_details(*color, p, sz, t)
            }
            ActorType::Rocket => Rocket::create(p, sz, t),
            ActorType::Bomb => Bomb::create(p, sz, t),
            ActorType::Water => {
                PlaceHolder::create_with_details(*self, p, sz, t)
            }
            ActorType::ExitDoor => ExitDoor::create(p, sz, t),
            ActorType::NoteBook => NoteBook::create(p, sz, t),
            ActorType::SurveillanceScreen => {
                SurveillanceScreen::create(p, sz, t)
            }
            ActorType::HostileShot(direction) => {
                HostileShot::create_with_details(*direction, p, sz, t)
            }
            ActorType::SodaFlying => SodaFlying::create(p, sz, t),
            ActorType::UnstableFloor => UnstableFloor::create(p, sz, t),
            ActorType::ExpandingFloor => ExpandingFloor::create(p, sz, t),
            ActorType::ConveyorRightEnd(direction) => {
                Conveyor::create_with_details(*direction, p, sz, t)
            }
            ActorType::Fan(direction) => {
                Fan::create_with_details(*direction, p, sz, t)
            }
            ActorType::StoneBackground => {
                PlaceHolder::create_with_details(*self, p, sz, t)
            }
            ActorType::Teleporter(index) => {
                Teleporter::create_with_details(*index, p, sz, t)
            }
            ActorType::FenceBackground => {
                PlaceHolder::create_with_details(*self, p, sz, t)
            }
            ActorType::Screen => {
                PlaceHolder::create_with_details(*self, p, sz, t)
            }
            ActorType::Item(item_type) => {
                Item::create_with_details(*item_type, p, sz, t)
            }
            ActorType::Balloon => Balloon::create(p, sz, t),
            ActorType::AccessCardSlot => AccessCardSlot::create(p, sz, t),
            ActorType::GloveSlot => GloveSlot::create(p, sz, t),
            ActorType::Key(color) => {
                Key::create_with_details(*color, p, sz, t)
            }
            ActorType::Keyhole(color) => {
                KeyHole::create_with_details(*color, p, sz, t)
            }
            ActorType::Door(color) => {
                Door::create_with_details(*color, p, sz, t)
            }
            ActorType::ShootableWall => ShootableWall::create(p, sz, t),
            ActorType::Elevator => Elevator::create(p, sz, t),
            ActorType::Acme => Acme::create(p, sz, t),
            ActorType::Fire(direction) => {
                Fire::create_with_details(*direction, p, sz, t)
            }
            ActorType::Mill => Mill::create(p, sz, t),
            ActorType::ElectricArc => ElectricArc::create(p, sz, t),
            ActorType::AccessCardDoor => AccessCardDoor::create(p, sz, t),
            ActorType::Spikes(spike_type) => {
                Spikes::create_with_details(*spike_type, p, sz, t)
            }
            ActorType::Score(score_type) => {
                Score::create_with_details(*score_type, p, sz, t)
            }
            ActorType::BackgroundAnimation(animation_type) => {
                BackgroundAnimation::create_with_details(
                    *animation_type,
                    p,
                    sz,
                    t,
                )
            }
        }
    }
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum ActorMessageType {
    OpenDoor(KeyColor),
    OpenDoorAccessCard,
    TeleportTo(TeleporterIndex),
    ExpandFloor,
    RemoveElectricArc,
}

#[derive(Default)]
pub struct ActorMessageQueue {
    pub messages: Vec<ActorMessageType>,
}

impl ActorMessageQueue {
    pub fn new() -> Self {
        ActorMessageQueue {
            messages: Vec::new(),
        }
    }

    pub fn push_back(&mut self, message: ActorMessageType) {
        self.messages.push(message);
    }
}

trait CreateActorWithDetails: Sized {
    type Details;

    fn create_with_details(
        details: Self::Details,
        position: Point,
        sizes: &dyn Sizes,
        tiles: &mut LevelTiles,
    ) -> Actor;
}

trait CreateActor: Sized {
    fn create(
        position: Point,
        sizes: &dyn Sizes,
        tiles: &mut LevelTiles,
    ) -> Actor;
}

pub struct ActParameters<'a> {
    pub tiles: &'a mut LevelTiles,
    pub hero: &'a mut Hero,
    pub game_commands: &'a mut dyn GameCommands,
    pub actor_message_queue: &'a mut ActorMessageQueue,
    pub play_state: &'a mut PlayState,
    pub sizes: &'a dyn Sizes,
}

pub struct ShotParameters<'a> {
    pub tiles: &'a mut LevelTiles,
    pub game_commands: &'a mut dyn GameCommands,
    pub hero: &'a mut Hero,
    pub actor_message_queue: &'a mut ActorMessageQueue,
    pub sizes: &'a dyn Sizes,
}

pub struct RenderParameters<'a> {
    pub renderer: &'a mut dyn Renderer,
    pub sizes: &'a dyn Sizes,
}

pub struct ReceiveMessageParameters<'a> {
    pub message: ActorMessageType,
    pub hero: &'a mut Hero,
    pub tiles: &'a mut LevelTiles,
    pub sizes: &'a dyn Sizes,
}

pub struct HeroInteractStartParameters<'a> {
    pub play_state: &'a mut PlayState,
    pub hero: &'a mut Hero,
    pub game_commands: &'a mut dyn GameCommands,
    pub info_message_queue: &'a mut InfoMessageQueue,
    pub actor_message_queue: &'a mut ActorMessageQueue,
}

pub struct HeroInteractEndParameters<'a> {
    pub hero: &'a mut Hero,
    pub play_state: &'a mut PlayState,
}

pub(crate) trait ActorExt: std::fmt::Debug {
    fn hero_can_interact(&self, _hero: &Hero) -> bool {
        false
    }

    fn hero_interact_start(&mut self, _p: HeroInteractStartParameters) {}

    fn hero_interact_end(&mut self, _p: HeroInteractEndParameters) {}

    fn act(&mut self, p: ActParameters);

    fn render(&mut self, p: RenderParameters) -> Result<()>;

    fn can_get_shot(&self) -> bool {
        false
    }

    fn shot(&mut self, _p: ShotParameters) -> ShotProcessing {
        ShotProcessing::Ignore
    }

    fn position(&self) -> Rect;

    fn is_in_foreground(&self) -> bool;

    fn hurts_hero(&self, _hero: &Hero) -> bool {
        false
    }

    fn is_alive(&self) -> bool {
        true
    }

    fn acts_while_invisible(&self) -> bool {
        false
    }

    fn can_receive_message(
        &self,
        _message_type: ActorMessageType,
    ) -> bool {
        false
    }

    fn receive_message(&mut self, _p: ReceiveMessageParameters) {}

    fn background_tile_strategy(&self) -> BackgroundTileStrategy {
        BackgroundTileStrategy::KeepEmpty
    }
}
